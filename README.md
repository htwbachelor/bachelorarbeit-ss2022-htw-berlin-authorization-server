## Bitte die Reihenfolge zur Ausführung des Projekts einhalten!!!
1- Authorization Server
2- Resource Server
3- Client Server

#### Um den Authorization Server auszuführen:
1. In der Docker-compose.yml Datei des Projekts "MySql" Container starten. 
*Wenn nötig, die Environment-Variables, z. b. das Passwort und den Nutzername, ändern
2. „AuthorizationServerApplication“-Klasse ausführen.


#### Um den Resource Server auszuführen:
1. In der In der Docker-compose.yml Datei des Projekts "MongoDB" Container starten. 
*Wenn nötig, die Environment-Variables, z. b. das Passwort und den Nutzername, ändern.
2. In der In der Docker-compose.yml Datei des Projekts "Rabbitmq" starten
3. ConfigServerApplication-Klasse ausführen
4. ErurekaServerApplication-Klasse ausführen
5. GatewayServerApplication-Klasse ausführen
6. ChartServiceApplication-Klasse ausführen
7. DataServiceApplication-Klasse ausführen
8. ChartCompositeServiceApplication-Klasse ausführen


#### Um den Client Server auszuführen:
1. ClientServerApplication-Klasse ausführen

Homepage Url:
http://localhost:8080/




